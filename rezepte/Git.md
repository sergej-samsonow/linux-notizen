#### Konfiguration default branch anzeigen

```sh
git config --global init.defaultBranch master
```

#### In Gitlab Repository erstellen (es wird davon ausgegangen default branch ist master)

```sh
mkdir projekt-1
cd projekt-1
git init
echo "#### Projekt-1" > README.md
git add .
git commit -m "Intial commit"
git remote add origin git@gitlab.com:username/repository-name.git
git push --set-upstream origin master
```

#### Git remot url anzeigen

```
git config --get remote.origin.url
```

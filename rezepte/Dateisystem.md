#### LVM logical volume verkleinern

Es gibt kein einfacher Weg, um XFS System zu verkleinern (ext Dateisysteme auch nicht). Mögliche Herangehensweise

- Daten Sichern
- Mountpoint unmounten
- Volumen verkleinern
- Neu formatieren
- Mountpoint wieder binden
- Backup einspielen

Beispiel für XFS Datsystem:
    
```sh
xfsdump -J - /path | gzip > path.backup.gz
umount /path
lvreduce -L -20G /dev/mapper/vg00-lv00
mkfs.xfs -v /dev/mapper/vg00-lv00
mount /dev/mapper/vg00-lv00 /path
gunzip -c path.backup.gz | xfsrestore -J - /path
```

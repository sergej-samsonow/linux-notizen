#### Firefox Profile erstellen

Für bestimmte Webseiten macht es Sinn, eigen Firefox Profile anzulegen.
Beispiel für Application **MyApp**.

- Verzeichnis mit Firefox Profil Daten erstellen
- Profil für MyApp Applikation in Firefox registrieren

```sh
firefoxProfilePath="$(cd ~ && pwd)/.firefox/MyApp"
mkdir -p $firefoxProfilePath
firefox -CreateProfile "MyApp $firefoxProfilePath"
```

#### Firefox mit bestimmten Profil ausführen

Firefox mit Profil **MyApp** ausführen

```sh
firefox -new-instance -P MyApp
```

#### Eigenen angepassten Starter für Gnome 3 anlegen

**ACHTUNG!** Auch wenn man eigenen angepassten Starter erstellt,
 wird es trotzt dem bei Ausführung als Firefox Instanz in Gnome 3 Dash angezeigt.
[Eigentlich soll ```StartupWMClass``` genau das verhindern, funktioniert unter
Wayland aber nicht mehr.](https://askubuntu.com/a/1468539)
 
```sh
applicatonStarterPath="$(cd ~ && pwd)/.local/share/applications"
cat << EOF > "$applicatonStarterPath/MyApp.desktop"
Encoding=UTF-8
Name=MyApp
Exec=firefox -new-instance -P MyApp --class FirefoxMyApp
Terminal=false
Type=Application
Icon=$applicatonStarterPath/MyAppIcon.png
Type=Application
Categories=Development
StartupWMClass=FirefoxMyApp
EOF
```

 
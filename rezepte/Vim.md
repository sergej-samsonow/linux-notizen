#### Minimale Vim Konfiguration

```vim
colorscheme desert

" Vim Verzeichnise 
set undodir=~/.vim/undo//
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//

" Tabs
set tabstop=4
set shiftwidth=4
set expandtab

" Statusline
set laststatus=2
set statusline=
set statusline+=%m
set statusline+=\ 
set statusline+=%F
set statusline+=%=
set statusline+=%l
set statusline+=/ 
set statusline+=%L
set statusline+=\ 
set statusline+=\ 
set statusline+=%c
set statusline+=\ 
set statusline+=\ 
set statusline+=%{strlen(&fenc)?&fenc:'none'}
set statusline+=\ 
set statusline+=%{&ff}

" Undo dirs 
silent! call mkdir($HOME."/".$USERNAME."/.vim/backup", "p")
silent! call mkdir($HOME."/".$USERNAME."/.vim/swap", "p")
silent! call mkdir($HOME."/".$USERNAME."/.vim/undo", "p")
set backup
set swapfile
set undofile
set directory=$HOME/.vim/swap//
set backupdir=$HOME/.vim/backup//
set undodir=$HOME/.vim/undo//

" Windows Einstellungen

set guifont=Lucida_Console:h8

set undodir=C:\Users\name\vim\undo//
set backupdir=C:\Users\name\vim\backup//
set directory=C:\Users\name\vim\swap//
```

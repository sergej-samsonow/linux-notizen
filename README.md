# linux-notizen
Notizen und Schnipsel rundum Linux Betriebssystem

Markdown Syntax [Basic - https://www.markdownguide.org/basic-syntax/](https://www.markdownguide.org/basic-syntax/)

Markdown Syntax [Extended - https://www.markdownguide.org/extended-syntax/](https://www.markdownguide.org/extended-syntax/)

Markdown Emojis [Emojis - https://gist.github.com/rxaviers/7360908](https://gist.github.com/rxaviers/7360908)


## DNS
Yandex DNS 

| Typ		| Primary IP	| Secondary IP	|
|---------------|---------------|---------------|
| Einfach	| 77.88.8.8	| 77.88.8.1	|
| Sicher	| 77.88.8.88	| 77.88.8.2	|
| Filter	| 77.88.8.7	| 77.88.8.3	|
